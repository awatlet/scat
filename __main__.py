import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import datetime
import os
import configparser

from SCat.catalogue import *
from SCat.utils import *
from SCat.plots import *

if __name__=='__main__':

    # print(sys.argv[1])

    config = configparser.ConfigParser(inline_comment_prefixes='#')
    if len(sys.argv) == 1:
        config.read(r'C:\Users\arnw\Documents\projects\z_Other\Nepal\catalogue.ini')
    else:
        config.read(sys.argv[1])
    section = 'Catalogue'
    for key in config[section]:
        if config[section][key][0] == '[' or config[section][key][0].isdigit():
            exec("%s = %s" % (key, config[section][key]))
        else:
            if '\\' in config[section][key]:
                config[section][key] = config[section][key].replace("\\", "\\\\")
            exec("%s = '%s'" % (key, config[section][key]))
            exec("""%s = %s.replace('"', '')""" % (key, key))
    sys.stdout.write('Analysing catalogue os.path.join(main_dir, file')
    df = pd.read_csv(os.path.join(main_dir, file), sep=separator, header=header)
    my_catalogue = catalogue(df, time_column=column, time_units=time_units, name=file[:-4])

    section = 'Randomisation'
    for key in config[section]:
        if config[section][key][0] == '[' or config[section][key][0].isdigit():
            exec("%s = %s" % (key, config[section][key]))
        else:
            exec("%s = '%s'" % (key, str(config[section][key])))
    my_catalogue.randomisation(bin_range=bin_range, bin_increment=bin_increment, n_ran=n_ran, column=column,
                               verbose=verbose)

    section = 'Reshuffling'
    for key in config[section]:
        if config[section][key][0] == '[' or config[section][key][0].isdigit():
            exec("%s = %s" % (key, config[section][key]))
        else:
            exec("%s = '%s'" % (key, str(config[section][key])))
    my_catalogue.reshuffling(dt_n=dt_n, column=column, n_ran=n_stack, verbose=True)


    if config['Catalogue'].getboolean('dt_max_lookup'):
        section = 'Reshuffling'
        for key in config[section]:
            if config[section][key][0] == '[' or config[section][key][0].isdigit():
                exec("%s = %s" % (key, config[section][key]))
            else:
                exec("%s = '%s'" % (key, config[section][key]))
                exec("""%s = %s.replace('"', '')""" % (key, key))
        my_catalogue.search_for_dt_max(dt_max_range=reshuffling_range, dt_max_increment=reshuffling_increment,
                                       seed=seed,
                                       dt_max_n_stack=n_stack, column=column, bin_size_factor=bin_size_factor,
                                       verbose=verbose,
                                       save=config[section].getboolean('save'), output=output)

        if ('Dt_max' in plots or 'all' in plots) and 'None' not in plots:

            section = 'Dt_max plot'
            for key in config[section]:
                if config[section][key][0] == '[' or config[section][key][0].isdigit():
                    exec("%s = %s" % (key, config[section][key]))
                else:
                    exec("%s = '%s'" % (key, config[section][key]))
                    exec("""%s = %s.replace('"', '')""" % (key, key))
            if figsize == 'auto':
                figsize = None
            fig, ax = plt.subplots(figsize=figsize)
            plot_dt_max(my_catalogue, ax=ax, fig=fig, color=color, xlim=xlim, ylim=ylim,
                           loc_legend=loc_legend,
                           save=config[section].getboolean('save'), output=output, main_dir=main_dir)


    if ('Triggershocks' in plots or 'all' in plots) and 'None' not in plots:
        section = 'Triggershocks plot'
        for key in config[section]:
            if config[section][key][0] == '[' or config[section][key][0].isdigit():
                exec("%s = %s" % (key, config[section][key]))
            else:
                exec("%s = '%s'" % (key, str(config[section][key])))
        if figsize == 'auto':
            figsize = None
        fig, ax = plt.subplots(figsize=figsize)
        plot_triggershocks(my_catalogue, dt=dt, ax=ax, fig=fig, color=color, xlim=xlim, ylim=ylim,
                           loc_legend=loc_legend,
                           save=config[section].getboolean('save'), output=output, main_dir=main_dir)
        plt.draw()

    if ('Bars' in plots or 'all' in plots) and 'None' not in plots:
        section = 'Bars plot'
        for key in config[section]:
            if config[section][key][0] == '[' or config[section][key][0] == '(' or config[section][key][0].isdigit():
                exec("%s = %s" % (key, config[section][key]))
            else:
                exec("%s = '%s'" % (key, config[section][key]))
                exec("""%s = %s.replace('"', '')""" % (key, key))

        if dt_n == 'dt_max':
            dt_n = my_catalogue.dt_max
        if figsize == 'auto':
            figsize = None
        fig, ax = plt.subplots(figsize=figsize)
        plot_bars(my_catalogue, ax=ax, fig=fig, color=color, xlim=xlim, ylim=ylim, loc_legend=loc_legend,
                  save=config[section].getboolean('save'), output=output, main_dir=main_dir)
        plt.draw()

    if ('Dt*_loglog' in plots or 'all' in plots) and 'None' not in plots:
        section = 'Dt*_loglog plot'
        for key in config[section]:
            if config[section][key][0] == '[' or config[section][key][0].isdigit():
                exec("%s = %s" % (key, config[section][key]))
            else:
                exec("%s = '%s'" % (key, str(config[section][key])))
        fig, ax = plt.subplots()
        plot_dt_star_loglog(my_catalogue, ax=ax, fig=fig, color=color, xlim=xlim, ylim=ylim, loc_legend=loc_legend,
                            save=config[section].getboolean('save'), output=output, main_dir=main_dir)
        plt.draw()

    if ('Dt*_loglin' in plots or 'all' in plots) and 'None' not in plots:
        section = 'Dt*_loglin plot'
        for key in config[section]:
            if config[section][key][0] == '[' or config[section][key][0].isdigit():
                exec("%s = %s" % (key, config[section][key]))
            else:
                exec("%s = '%s'" % (key, str(config[section][key])))
        if figsize == 'auto':
            figsize = None
        fig, ax = plt.subplots(figsize=figsize)
        plot_dt_star_loglin(my_catalogue, ax=ax, fig=fig, color=color, xlim=xlim, ylim=ylim, loc_legend=loc_legend,
                            save=config[section].getboolean('save'), output=output, main_dir=main_dir)
        plt.draw()

    # interactive(False)
    plt.show()
