import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import glob
import datetime
import os
import configparser

from SCat.catalogue import *
from SCat.utils import *

def plot_triggershocks(catalogue, dt=100, ax=None, fig=None, color=None, xlim='auto', ylim='auto', loc_legend='best',
                        save=False, output='auto', main_dir='.'):
    if fig is None:
        fig, ax = plt.subplots()
    catalogue.set_triggershocks(threshold=dt)
    df = catalogue.df
    ax.set_title('%i days threshold selection' % dt)
    ax.plot(df['time'], np.arange(0, len(df.index), 1), '.', color=color, label=catalogue.name)  # 105 days filtering')
    ax.plot(df[df['main'] == 1]['time'], np.arange(0, len(df.index), 1)[np.where(df['main'] == 1)], 'r.',
            label='Trigger shocks')
    if xlim != 'auto':
        ax.set_xlim(xlim)
    if ylim != 'auto':
        ax.set_ylim(ylim)
    ax.legend(loc=loc_legend)
    if save:
        if output == 'auto':
            output_name = 'triggershocks_%s_reshuffled_%i' % (catalogue.name, catalogue.n_ran)
        else:
            output_name = output
        fig.savefig(os.path.join(main_dir, '%s.png' % output_name), dpi=(400), bbox_inches='tight')
        fig.savefig(os.path.join(main_dir, '%s.pdf' % output_name), dpi=(400), bbox_inches='tight')

def plot_bars(catalogue, ax=None, fig=None, color='blue', xlim='auto', ylim='auto', loc_legend='best',
                        save=False, output='auto', main_dir='.'):
    if fig is None:
        fig, ax = plt.subplots()
    ax.bar(catalogue.random_df.columns, catalogue.random_df.mean(axis=0).values, width=catalogue.dt_n - catalogue.dt_n / 8,
           label='Reshuffled catalogue dt<%i years' % catalogue.dt_n, color='gray', yerr=2 * catalogue.random_df.std(axis=0).values)
    ax.bar(catalogue.stacked_df.columns, catalogue.stacked_df.sum(axis=0).values, width=catalogue.dt_n - catalogue.dt_n / 2, color=color,
           label='Observed %s catalogue' % catalogue.name)
    ax.set_ylabel('Number of earthquakes')
    if xlim != 'auto':
        ax.set_xlim(xlim)
    if ylim != 'auto':
        ax.set_ylim(ylim)
    ax.legend(loc=loc_legend)
    if save:
        if output == 'auto':
            output_name = 'bars_%s_reshuffled_%i' % (catalogue.name, catalogue.n_ran)
        else:
            output_name = output
        fig.savefig(os.path.join(main_dir, '%s.png' % output_name), dpi=(400), bbox_inches='tight')
        fig.savefig(os.path.join(main_dir, '%s.pdf' % output_name), dpi=(400), bbox_inches='tight')

def plot_dt_star_loglin(catalogue, ax=None, fig=None, color='blue', xlim='auto', ylim='auto', loc_legend='best',
                        save=False, output='auto', main_dir='.'):
    if fig is None:
        fig, ax = plt.subplots()
    dt_star_ran = (catalogue.dt_ran.dropna() / catalogue.dt_ran.dropna().max(axis=0))
    to_plot_ran = catalogue.ddt_ran
    to_plot = catalogue.dt
    for col in to_plot_ran.columns:
        ax.plot(dt_star_ran[col].values, range(1, len(dt_star_ran.index) + 1, 1), '-', alpha=.1, color='gray',
                linewidth=.5)
    ax.plot(to_plot / to_plot.max(axis=0), range(1, len(to_plot.index) + 1, 1), '+-', color=color,
            label='Observed %s catalogue' % catalogue.name)

    df_sig_min = dt_star_ran.mean(axis=1) - 2 * dt_star_ran.std(axis=1)
    df_sig_min[df_sig_min < 0] = 1e-6
    df_sig_max = dt_star_ran.mean(axis=1) + 2 * dt_star_ran.std(axis=1)
    df_sig_max[df_sig_max < 0] = 1e-6
    ax.plot(dt_star_ran.mean(axis=1), range(1, len(dt_star_ran.index) + 1, 1), '--', color='dimgray',
            label='Reshuffled catalogue n=%i' % catalogue.n_ran)
    ax.plot(df_sig_max, range(1, len(dt_star_ran.index) + 1, 1), ':', color='dimgray',
            label='2$\sigma$ reshuffled catalogue')
    ax.plot(df_sig_min, range(1, len(dt_star_ran.index) + 1, 1), ':', color='dimgray')

    ax.set_ylabel("DDt")
    ax.set_yscale('log')
    ax.set_xlabel("$\Delta$t*")
    ax.set_ylabel("N(m)")
    if xlim != 'auto':
        ax.set_xlim(xlim)
    if ylim != 'auto':
        ax.set_ylim(ylim)
    ax.legend(loc=loc_legend)
    if save:
        if output == 'auto':
            output_name = 'Dt_Star_loglin_%s_reshuffled_%i' % (catalogue.name, catalogue.n_ran)
        else:
            output_name = output
        fig.savefig(os.path.join(main_dir, '%s.png' % output_name), dpi=(400), bbox_inches='tight')
        fig.savefig(os.path.join(main_dir, '%s.pdf' % output_name), dpi=(400), bbox_inches='tight')
    fig.canvas.draw_idle()

def plot_dt_star_loglog(catalogue, ax=None, fig=None, color=None, xlim='auto', ylim='auto', loc_legend='best',
                        save=False, output='auto', main_dir='.',random=True,normalise=False, dt_mean=False,
                        display_std_mean=True):
    if fig is None:
        fig, ax = plt.subplots()
    if random:
        if dt_mean:
            dt_star_ran = (catalogue.dt_ran.dropna() / catalogue.dt_ran.dropna().mean(axis=0))
        else:
            dt_star_ran = (catalogue.dt_ran.dropna() / catalogue.dt_ran.dropna().max(axis=0))

        to_plot_ran = catalogue.ddt_ran
        to_plot = catalogue.dt
        if normalise:
            norm = np.linspace(0,1,dt_star_ran.shape[0])
        else:
            norm = np.arange(1,dt_star_ran.shape[0]+1,1)
        for col in to_plot_ran.columns:
            # if normalise:
            #     ax.plot(dt_star_ran[col].values, np.linspace(0,1,dt_star_ran.shape[0]), '-', alpha=.1, color='gray', linewidth=.5)
            # else:
            ax.plot(dt_star_ran[col].values, norm, '-', alpha=.1, color='gray', linewidth=.5)

        if display_std_mean:
            df_sig_min = dt_star_ran.mean(axis=1) - 2 * dt_star_ran.std(axis=1)
            df_sig_min[df_sig_min < 0] = 1e-6
            df_sig_max = dt_star_ran.mean(axis=1) + 2 * dt_star_ran.std(axis=1)
            df_sig_max[df_sig_max < 0] = 1e-6
            ax.plot(dt_star_ran.mean(axis=1), norm, '--', color='dimgray',
                    label='Reshuffled catalogue n=%i' % catalogue.n_ran)
            ax.plot(df_sig_max, norm, ':', color='dimgray',
                    label='2$\sigma$ reshuffled catalogue')
            ax.plot(df_sig_min, norm, ':', color='dimgray')
    if normalise:
        norm = np.linspace(0,1,to_plot.shape[0])
    else:
        norm = np.arange(1, len(to_plot.index)+1, 1)
    if dt_mean:
        ax.plot(to_plot / to_plot.mean(axis=0), norm, '+-', color=color,
                label='Observed %s catalogue' % catalogue.name)
    else:
        ax.plot(to_plot / to_plot.max(axis=0), norm, '+-', color=color,
             label='Observed %s catalogue' % catalogue.name)
    ax.set_ylabel("DDt")
    ax.set_yscale('log')
    ax.set_xscale('log')
    if dt_mean:
        ax.set_xlabel("$\Delta$t/|$\Delta$t|")
    else:
        ax.set_xlabel("$\Delta$t*")
    ax.set_ylabel("N(m)")
    if xlim != 'auto':
        ax.set_xlim(xlim)
    if ylim != 'auto':
        ax.set_ylim(ylim)
    ax.legend(loc=loc_legend)
    if save:
        if output == 'auto':
            output_name = 'Dt_Star_loglin_%s_reshuffled_%i' % (catalogue.name, catalogue.n_ran)
        else:
            output_name = output
        fig.savefig(os.path.join(main_dir, '%s.png' % output_name), dpi=(400), bbox_inches='tight')
        fig.savefig(os.path.join(main_dir, '%s.pdf' % output_name), dpi=(400), bbox_inches='tight')
    fig.canvas.draw_idle()

def plot_dt_max(catalogue, ax=None, fig=None, color='blue', xlim='auto', ylim='auto', loc_legend='best',
                        save=False, output='auto', main_dir='.'):
    if fig is None:
        fig, ax = plt.subplots()

    df_stack_random = catalogue.reshuffled
    ax.plot(df_stack_random.index, df_stack_random.stack_sum, '-', markersize=2, color=color,
            label='Stacked events in main bin - %s' % my_catalogue.name)
    ax.plot(df_stack_random.index, df_stack_random.random_mean, '-', markersize=2, color='gray',
            label='Stacked events in main bin - Reshuffled catalogue')
    ax.fill_between(df_stack_random.index,
                    (df_stack_random.random_mean + 2 * df_stack_random.random_std).astype(float),
                    (df_stack_random.random_mean - 2 * df_stack_random.random_std).astype(float),
                    color='lightgray',
                    label='2$\sigma$ Reshuffled catalogue')
    ax.legend(loc=loc_legend)
    ax.set_xlabel('Dt bin width (yrs)')
    ax.set_ylabel('Stacked events')
    if xlim != 'auto':
        ax.set_xlim(xlim)
    if ylim != 'auto':
        ax.set_ylim(ylim)

    if save:
        if output == 'auto':
            output_name = 'dt_max_bin_%s_reshuffled_%i' % (my_catalogue.name, n_stack)
        else:
            output_name = output
        fig.savefig(os.path.join(main_dir, '%s.png' % output_name), dpi=(400), bbox_inches='tight')
        fig.savefig(os.path.join(main_dir, '%s.pdf' % output_name), dpi=(400), bbox_inches='tight')