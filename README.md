# Seismic Catalogue analysis (SCat)

SCat is a package designed to analyse statistically seismic catalogues using reshuffling and randomisation
approaches. It is still under developments...

## Running the code through command line
In order to run the code through command line, you will have to update your environment variables to add the directory
in which you store the package.
In Windows, go to Settings, search for "Edit the environment variables", click "Environment Variables" and add
C:/where/you/store/SCat to the PATH variable.

If you are under Linux, I guess you know how to update your PATH variable...

## Running the package
Simply run fron the command line (either cmd or anaconda prompt if using separate environments):

`python -m SCat catalogue.ini`

## Settings
All the settings used in the analyses and the plots creation are stored in the catalogue.ini file (you can rename it
as long as you point to it when calling the package).

See example/catalogue.ini

## Dependencies
You must have python 3.6 or above installed (preferably using anaconda, and even most preferably using a separate
environment in anaconda).

To install a spearate environment using anaconda simply run the following commands in anaconda prompt:

`conda create -n myenv python=3.7`

`conda activate myenv`

`conda install pandas numpy matplotlib`