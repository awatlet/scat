import pandas as pd
import numpy as np
import os
from SCat.utils import *


class catalogue():
    def __init__(self, catalogue_df=None, time_column=None, time_units='years', date_format=None, name='catalogue'):
        self.df = catalogue_df
        if time_units == 'years':
            self.time_units = 'years'
            self.df['time_rel'] = (self.df[time_column] - self.df[time_column].min())
        elif time_units == 'days':                          #TODO: hours => For some catalogues hours should be better
            self.time_units = 'days'
            self.df['time_rel'] = (self.df[time_column] - self.df[time_column].min())
        elif time_units == 'date':
            self.time_units = 'days'
            self.df[time_column] = pd.to_datetime(self.df[time_column], format=date_format)
            self.df['time_rel'] = (
                        (self.df[time_column] - self.df[time_column].min()) / np.timedelta64(3600 * 24, 's')).astype(
                float)
        self.df = self.df.sort_values('time_rel', ascending=True)
        self.df.set_index(np.arange(1, self.df.shape[0] + 1), inplace=True)
        self.variability = set_variability(self.df, column='time_rel')
        self.name = name
        self.reshuffled = None
        self.dt_max = None

    def set_mainshocks(self, threshold=250, prior_threshold=None, column='time_rel', first=1):
        if prior_threshold is None:
            prior_threshold = threshold
        self.df['trigger'] = np.zeros(len(self.df.index))
        for i in range(len(df.index) - 1):
            if self.df.iloc[i + 1].time_rel - self.df.iloc[i].time_rel <= threshold and self.df.iloc[i].time_rel - \
                    self.df.iloc[i - 1].time_rel >= prior_threshold:
                self.df['trigger'].iloc[i] = 1
        self.df['trigger'].iloc[0] = first
        self.triggershock_threshold = threshold

    def set_triggershocks(self, threshold=250, column='time_rel', first=1):
        self.df['main'] = np.zeros(len(self.df.index))
        for i in range(len(self.df.index) - 1):
            if self.df.iloc[i + 1].time_rel - self.df.iloc[i].time_rel <= threshold:
                self.df.loc[self.df.index[i + 1], 'main'] = 1
        self.mainshock_threshold = threshold

    def search_for_dt_max(self, dt_max_range=[1, 20], dt_max_increment=1, dt_max_n_stack=50, seed=True,
                          column='time_rel', bin_size_factor=10, verbose=False, save=False, output='auto',
                          main_dir='.'):
        if seed:
            random_series = random_catalogue_series(self.df, column=column, n_random=dt_max_n_stack,
                                                    seed=True)  ### Planting a random seed
        dt_max_range = np.arange(dt_max_range[0], dt_max_range[1], dt_max_increment)
        df_stack_random = pd.DataFrame(index=dt_max_range,
                                       columns=['stack_sum', 'random_mean', 'random_std', 'distance_to_random'])

        if verbose:
            sys.stdout.write('Initiating dt_max lookup\n')
        for bin_n in dt_max_range:
            if verbose:
                erase = '\x1b[1A\x1b[2K'
                sys.stdout.write(erase+'Reaching dt: %i\n'%bin_n)
            self.set_triggershocks(threshold=bin_n, column=column)
            if seed:
                random_df = stack_randomisation_series(self.df, random_series, bin_width=bin_n,
                                                   bin_range=[-bin_n * bin_size_factor, bin_n * bin_size_factor],
                                                       n_stack=dt_max_n_stack, column=column, verbose= verbose)
            else:
                random_df = stack_randomisation(self.df, bin_width=bin_n, bin_range=[-bin_n * bin_size_factor,
                                                                                     bin_n * bin_size_factor],
                                                n_stack=dt_max_n_stack, column=column, verbose= verbose)
            stacked_df = stack(self.df, bin_width=bin_n, bin_range=[-bin_n * bin_size_factor, bin_n * bin_size_factor],
                               column=column)
            df_stack_random.loc[bin_n]['stack_sum'] = stacked_df.sum(axis=0)[bin_n / 2.]
            df_stack_random.loc[bin_n]['random_mean'] = random_df.mean()[bin_n / 2]
            df_stack_random.loc[bin_n]['random_std'] = random_df.std()[bin_n / 2]
            df_stack_random.loc[bin_n]['distance_to_random'] = stacked_df.sum(axis=0)[bin_n / 2.] - (
                    random_df.mean()[bin_n / 2] + random_df.std()[bin_n / 2] * 2)
        if verbose:
            sys.stdout.write('dt_max lookup done\n')
        self.reshuffled = df_stack_random
        self.reshuffled = self.reshuffled.astype(float)
        if save:
            if output == 'auto':
                output_name = os.path.join(main_dir,'%s_reshuffled_df'%self.name)
            else:
                output_name = os.path.join(main_dir,output)
            self.reshuffled.to_csv(output_name)
            if verbose:
                sys.stdout.write('File %s saved'%output_name)

        if self.reshuffled[self.reshuffled.distance_to_random > 0].shape[0]>0:
            self.dt_max = self.reshuffled[self.reshuffled.distance_to_random > 0].distance_to_random.idxmin()
        else:
            sys.stderr.write('!!! No dt max found with positive distance, assign closest guess... '
                             'print(catalogue.reshuffled) to double check\n')
            self.dt_max = self.reshuffled[self.reshuffled.distance_to_random < 0].distance_to_random.idxmax()

        sys.stdout.write('dt_max found is %f'%self.dt_max)

    def randomisation(self, bin_range=[1,100], bin_increment=10, n_ran=1000, column='time_rel', verbose=False):
        bin_n_range = np.arange(bin_range[0], bin_range[1], bin_increment)
        dt = dt_diff(self.df, column=column)
        dt_ran = dt_diff_random(self.df, column=column, verbose=verbose)
        df_ddt_ran, bins = ddt_random(self.df, bins=bin_n_range, n_ran=n_ran, column=column, verbose=verbose)
        self.n_ran = n_ran
        self.dt = dt
        self.dt_ran = dt_ran
        self.ddt_ran = df_ddt_ran

    def reshuffling(self,dt_n=100,column='time_rel',n_ran=1000,bin_size_factor=10, seed=True, verbose=False):
        self.set_triggershocks(threshold=dt_n, column=column)
        random_series = random_catalogue_series(self.df, column=column, n_random=n_ran, seed=seed, verbose=verbose)
        random_df = stack_randomisation_series(self.df, random_series, bin_width=dt_n,
                                               bin_range=[-dt_n * bin_size_factor, dt_n * bin_size_factor],
                                               n_stack=n_ran, column=column, verbose=verbose)
        stacked_df = stack(self.df, bin_width=dt_n, bin_range=[-dt_n * bin_size_factor, dt_n * bin_size_factor],
                           column=column)
        self.stacked_df = stacked_df
        self.random_df = random_df
        self.n_ran = n_ran
        self.dt_n = dt_n
