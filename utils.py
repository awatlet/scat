import pandas as pd
import numpy as np
import sys

def set_variability(df, column='time_d'):
    n = df[column].std() / df[column].mean()
    return n

### simple stacking with a bin and a bin range (excluding mainshocks from the stack)
def stack(df, bin_width=200, bin_range=[-2000, 2000], column='WM Age AD'):
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for i in range(len(df.index)):
        if df.iloc[i].main == 1:
            big = df.iloc[i][column] - df[column].min()
            df['deltas'] = df[column] - df[column].min() - big
            #         print(np.histogram(df['deltas'],bins=np.arange(-2000,2200,100)))
            stacked_df = stacked_df.append(pd.DataFrame([np.histogram(df[df.deltas != 0]['deltas'],
                                                                      bins=np.arange(bin_range[0],
                                                                                     bin_range[1] + bin_width,
                                                                                     bin_width))[0]],
                                                        columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                          bin_range[1] + bin_width / 2., bin_width)))
    return stacked_df


def stack_main(df, bin_width=200, bin_range=[-2000, 2000], column='WM Age AD'):
    #     df['age_rel'] =  df[column]-df[column].min()
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for i in range(len(df.index) - 1):
        if df.iloc[i + 1]['column'] - df.iloc[i]['column'] <= bin_width:
            big = df.iloc[i][column] - df[column].min()
            df['deltas'] = df[column] - df[column].min() - big
            #         print(np.histogram(df['deltas'],bins=np.arange(-2000,2200,100)))
            stacked_df = stacked_df.append(pd.DataFrame([np.histogram(df[df.deltas != 0]['deltas'],
                                                                      bins=np.arange(bin_range[0],
                                                                                     bin_range[1] + bin_width,
                                                                                     bin_width))[0]],
                                                        columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                          bin_range[1] + bin_width / 2., bin_width)))
    return stacked_df


def stack_main_series(df, df_random, bin_width=200, bin_range=[-2000, 2000], column='time_rel'):
    #     print(df)
    #     df['age_rel'] =  df[column]-df[column].min()
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for i in range(len(df.index) - 1):
        if df.iloc[i + 1][column] - df.iloc[i][column] <= bin_width:
            big = df_random.iloc[i] - df_random.min()
            df_random['deltas'] = df_random - df_random.min() - big
            #         print(np.histogram(df['deltas'],bins=np.arange(-2000,2200,100)))
            stacked_df = stacked_df.append(pd.DataFrame([np.histogram(df_random[df_random.deltas != 0]['deltas'],
                                                                      bins=np.arange(bin_range[0],
                                                                                     bin_range[1] + bin_width,
                                                                                     bin_width))[0]],
                                                        columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                          bin_range[1] + bin_width / 2., bin_width)))
    return stacked_df


### doing the same for a random series... (not very useful...) TODO: define a random df before and use the stack function only
def stack_random(df, bin_width=200, bin_range=[-2000, 2000], column='time_rel'):
    df_random = df.copy()
    N = np.sort(np.random.uniform(df_random[column].min(), df_random[column].max(), len(df_random.index)))
    df_random[column] = N
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for i in range(len(df.index)):
        if df.iloc[i].main == 1:
            big = df_random.iloc[i][column] - df_random[column].min()
            df_random['deltas'] = df_random[column] - df_random[column].min() - big
            #         print(np.histogram(df['deltas'],bins=np.arange(-2000,2200,100)))
            stacked_df = stacked_df.append(pd.DataFrame([np.histogram(df_random[df_random.deltas != 0]['deltas'],
                                                                      bins=np.arange(bin_range[0],
                                                                                     bin_range[1] + bin_width,
                                                                                     bin_width))[0]],
                                                        columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                          bin_range[1] + bin_width / 2., bin_width)))

    return stacked_df


def stack_randomisation(df, bin_width=200, bin_range=[-2000, 2000], n_stack=100, column='time_rel'):
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for n in range(n_stack):
        random_df = stack_random(df, bin_width=bin_width, bin_range=bin_range, column=column)
        stacked_df = stacked_df.append(pd.DataFrame([random_df.sum(axis=0).tolist()],
                                                    columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                      bin_range[1] + bin_width / 2., bin_width)))
    return stacked_df


def stack_randomisation_series(df, df_random_series, bin_width=200, bin_range=[-2000, 2000], n_stack=100,
                               column='time_rel',verbose=False):
    stacked_df = pd.DataFrame(
        columns=np.arange(bin_range[0] + bin_width / 2., bin_range[1] + bin_width / 2., bin_width))
    for n in range(n_stack):
        if verbose:
            sys.stdout.write('Reaching stack {} of {} for stack_randomisation_series\r'.format(n+1,n_stack))
        df_tmp = df_random_series[df_random_series.index == n].transpose()
        random_df = stack_main_series(df, df_tmp, bin_width=bin_width, bin_range=bin_range, column=column)
        stacked_df = stacked_df.append(pd.DataFrame([random_df.sum(axis=0).tolist()],
                                                    columns=np.arange(bin_range[0] + bin_width / 2.,
                                                                      bin_range[1] + bin_width / 2., bin_width)))
    if verbose:
        sys.stdout.write('Stack done for stack_randomisation_series                \n')
    return stacked_df


def random_catalogue_series(df, n_random=1000, column='time_d', seed=False, verbose=False):
    df_random = pd.DataFrame(index=np.arange(0, n_random, 1), columns=np.arange(0, len(df.index), 1))
    for n in range(n_random):
        if seed:
            np.random.seed(n)
        if verbose:
            sys.stdout.write('Reaching stack {} of {} for random_catalogue_series\r'.format(n+1,n_random))
        df_random.iloc[n] = \
        pd.DataFrame(np.sort(np.random.uniform(df[column].min(), df[column].max(), len(df.index)))).transpose().iloc[0]
    if verbose:
        sys.stdout.write('Stack done for random_catalogue_series                   \n')
    return df_random

def dt_diff(df,column='time_d'):
    dt = df[column].diff()
    dt= dt.sort_values(ascending=False)
    dt.index = (range(len(dt.index)))
    return dt

def dt_diff_random(df,n_ran=1000,column='time_d', verbose=False):
    dt_diff_ran = pd.DataFrame(columns=range(n_ran))
    for n in range(n_ran):
        if verbose:
            sys.stdout.write('Reaching stack {} of {} for ddt_diff_random\r'.format(n+1,n_ran))
        df_ran = event_deltas_random_one(df,column=column)
        dt_tmp = dt_diff(df_ran,column=column)
#         print(i, ddt_tmp)
        dt_diff_ran[n] = dt_tmp
    if verbose:
        sys.stdout.write('Stack done for ddt_diff_random                         \n')
    return dt_diff_ran

def event_deltas_random_one(df,column='WM Age AD'):
    df_random = pd.DataFrame(index=range(len(df.index)))
    df_random[column] = np.sort(np.random.uniform(df[column].min(),df[column].max(),len(df.index)))
    return df_random

def ddt(df,bins=np.arange(-50,551,50),column='time_d'):
    dt = dt_diff(df,column=column)
    hist = np.histogram(dt.dropna().values,bins=bins)
    ddt = np.cumsum(hist[0][::-1])[::-1]
    return ddt,hist[1][:-1]

def ddt_random(df,bins=np.arange(-50,551,50),n_ran=1000,column='time_d', verbose=False):
    ddt_ran = pd.DataFrame(columns=range(n_ran))
    for n in range(n_ran):
        if verbose:
            sys.stdout.write('Reaching stack {} of {} for ddt_random\r'.format(n+1,n_ran))
        df_ran = event_deltas_random_one(df,column=column)
        ddt_tmp, bins_dt = ddt(df_ran,bins=bins,column=column)
#         print(i, ddt_tmp)
        ddt_ran[n] = ddt_tmp
    if verbose:
        sys.stdout.write('Stack done for ddt_random                              \n')
    return ddt_ran,bins_dt